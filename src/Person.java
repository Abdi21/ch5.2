class Person {
  public Person() {
   name = "Mohamed";
   birthdayYear = 1998; // my default  
  }
  public Person(String givenName, int yearOfBirth) { 
    name = givenName;
    birthdayYear = yearOfBirth; 
  }
  public String getName() { 
    return name; 
  }
  public String changeName(String name) { 
    String aux; 
    aux = this.name;
    this.name = name;
    return aux; 
  }
  public int getAgeInYears(int currentYear) { 
    return currentYear - birthdayYear;  
  }
  private String name;
  private int birthdayYear; 
  public static void main(String[] args) {
    Person a = new Person(); 
    Person b = new Person("His major " , 1918); 
    String name = a.changeName(" at Nova");     
    System.out.println(
      "Abdi " + name + " = " + 
      "19, is a student " + a.getName()    
    );
    System.out.println(
       b.getName() + "is " + 
       b.getAgeInYears(1945) + 
       " Computer Science. "
    );
  }  
}
